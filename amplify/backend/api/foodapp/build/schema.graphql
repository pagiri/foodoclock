type User {
  id: ID!
  userId: ID!
  FirstName: String!
  LastName: String
  SUID: Int!
  PhoneNumber: Int
  Email: String!
  Role: String
  schedule(filter: ModelScheduleFilterInput, sortDirection: ModelSortDirection, limit: Int, nextToken: String): ModelScheduleConnection
  complaint(filter: ModelComplaintFilterInput, sortDirection: ModelSortDirection, limit: Int, nextToken: String): ModelComplaintConnection
  createdAt: AWSDateTime!
  updatedAt: AWSDateTime!
}

type Schedule {
  id: ID!
  day: Int
  endTime: String
  month: Int
  startTime: String
  year: Int
  user: User
  createdAt: AWSDateTime!
  updatedAt: AWSDateTime!
}

type News {
  id: ID!
  newsid: ID!
  Date: String
  Content: String
  createdAt: AWSDateTime!
  updatedAt: AWSDateTime!
}

type Complaint {
  id: ID!
  complaintId: ID!
  Date: String
  content: String
  user: User
  createdAt: AWSDateTime!
  updatedAt: AWSDateTime!
}

type Feedback {
  id: ID!
  feedbackId: ID!
  Date: String
  content: String
  createdAt: AWSDateTime!
  updatedAt: AWSDateTime!
}

enum ModelSortDirection {
  ASC
  DESC
}

type ModelUserConnection {
  items: [User]
  nextToken: String
}

input ModelStringInput {
  ne: String
  eq: String
  le: String
  lt: String
  ge: String
  gt: String
  contains: String
  notContains: String
  between: [String]
  beginsWith: String
  attributeExists: Boolean
  attributeType: ModelAttributeTypes
  size: ModelSizeInput
}

input ModelIDInput {
  ne: ID
  eq: ID
  le: ID
  lt: ID
  ge: ID
  gt: ID
  contains: ID
  notContains: ID
  between: [ID]
  beginsWith: ID
  attributeExists: Boolean
  attributeType: ModelAttributeTypes
  size: ModelSizeInput
}

input ModelIntInput {
  ne: Int
  eq: Int
  le: Int
  lt: Int
  ge: Int
  gt: Int
  between: [Int]
  attributeExists: Boolean
  attributeType: ModelAttributeTypes
}

input ModelFloatInput {
  ne: Float
  eq: Float
  le: Float
  lt: Float
  ge: Float
  gt: Float
  between: [Float]
  attributeExists: Boolean
  attributeType: ModelAttributeTypes
}

input ModelBooleanInput {
  ne: Boolean
  eq: Boolean
  attributeExists: Boolean
  attributeType: ModelAttributeTypes
}

input ModelSizeInput {
  ne: Int
  eq: Int
  le: Int
  lt: Int
  ge: Int
  gt: Int
  between: [Int]
}

input ModelUserFilterInput {
  userId: ModelIDInput
  FirstName: ModelStringInput
  LastName: ModelStringInput
  SUID: ModelIntInput
  PhoneNumber: ModelIntInput
  Email: ModelStringInput
  Role: ModelStringInput
  and: [ModelUserFilterInput]
  or: [ModelUserFilterInput]
  not: ModelUserFilterInput
}

enum ModelAttributeTypes {
  binary
  binarySet
  bool
  list
  map
  number
  numberSet
  string
  stringSet
  _null
}

type Query {
  getUser(id: ID!): User
  listUsers(filter: ModelUserFilterInput, limit: Int, nextToken: String): ModelUserConnection
  getSchedule(id: ID!): Schedule
  listSchedules(filter: ModelScheduleFilterInput, limit: Int, nextToken: String): ModelScheduleConnection
  getNews(id: ID!): News
  listNewss(filter: ModelNewsFilterInput, limit: Int, nextToken: String): ModelNewsConnection
  getComplaint(id: ID!): Complaint
  listComplaints(filter: ModelComplaintFilterInput, limit: Int, nextToken: String): ModelComplaintConnection
  getFeedback(id: ID!): Feedback
  listFeedbacks(filter: ModelFeedbackFilterInput, limit: Int, nextToken: String): ModelFeedbackConnection
}

input CreateUserInput {
  id: ID
  userId: ID!
  FirstName: String!
  LastName: String
  SUID: Int!
  PhoneNumber: Int
  Email: String!
  Role: String
}

input UpdateUserInput {
  userId: ID
  FirstName: String
  LastName: String
  SUID: Int
  PhoneNumber: Int
  Email: String
  Role: String
}

input DeleteUserInput {
  id: ID
}

type Mutation {
  createUser(input: CreateUserInput!, condition: ModelUserConditionInput): User
  updateUser(input: UpdateUserInput!, condition: ModelUserConditionInput): User
  deleteUser(input: DeleteUserInput!, condition: ModelUserConditionInput): User
  createSchedule(input: CreateScheduleInput!, condition: ModelScheduleConditionInput): Schedule
  updateSchedule(input: UpdateScheduleInput!, condition: ModelScheduleConditionInput): Schedule
  deleteSchedule(input: DeleteScheduleInput!, condition: ModelScheduleConditionInput): Schedule
  createNews(input: CreateNewsInput!, condition: ModelNewsConditionInput): News
  updateNews(input: UpdateNewsInput!, condition: ModelNewsConditionInput): News
  deleteNews(input: DeleteNewsInput!, condition: ModelNewsConditionInput): News
  createComplaint(input: CreateComplaintInput!, condition: ModelComplaintConditionInput): Complaint
  updateComplaint(input: UpdateComplaintInput!, condition: ModelComplaintConditionInput): Complaint
  deleteComplaint(input: DeleteComplaintInput!, condition: ModelComplaintConditionInput): Complaint
  createFeedback(input: CreateFeedbackInput!, condition: ModelFeedbackConditionInput): Feedback
  updateFeedback(input: UpdateFeedbackInput!, condition: ModelFeedbackConditionInput): Feedback
  deleteFeedback(input: DeleteFeedbackInput!, condition: ModelFeedbackConditionInput): Feedback
}

input ModelUserConditionInput {
  userId: ModelIDInput
  FirstName: ModelStringInput
  LastName: ModelStringInput
  SUID: ModelIntInput
  PhoneNumber: ModelIntInput
  Email: ModelStringInput
  Role: ModelStringInput
  and: [ModelUserConditionInput]
  or: [ModelUserConditionInput]
  not: ModelUserConditionInput
}

type Subscription {
  onCreateUser: User @aws_subscribe(mutations: ["createUser"])
  onUpdateUser: User @aws_subscribe(mutations: ["updateUser"])
  onDeleteUser: User @aws_subscribe(mutations: ["deleteUser"])
  onCreateSchedule: Schedule @aws_subscribe(mutations: ["createSchedule"])
  onUpdateSchedule: Schedule @aws_subscribe(mutations: ["updateSchedule"])
  onDeleteSchedule: Schedule @aws_subscribe(mutations: ["deleteSchedule"])
  onCreateNews: News @aws_subscribe(mutations: ["createNews"])
  onUpdateNews: News @aws_subscribe(mutations: ["updateNews"])
  onDeleteNews: News @aws_subscribe(mutations: ["deleteNews"])
  onCreateComplaint: Complaint @aws_subscribe(mutations: ["createComplaint"])
  onUpdateComplaint: Complaint @aws_subscribe(mutations: ["updateComplaint"])
  onDeleteComplaint: Complaint @aws_subscribe(mutations: ["deleteComplaint"])
  onCreateFeedback: Feedback @aws_subscribe(mutations: ["createFeedback"])
  onUpdateFeedback: Feedback @aws_subscribe(mutations: ["updateFeedback"])
  onDeleteFeedback: Feedback @aws_subscribe(mutations: ["deleteFeedback"])
}

type ModelScheduleConnection {
  items: [Schedule]
  nextToken: String
}

input ModelScheduleFilterInput {
  id: ModelIDInput
  day: ModelIntInput
  endTime: ModelStringInput
  month: ModelIntInput
  startTime: ModelStringInput
  year: ModelIntInput
  and: [ModelScheduleFilterInput]
  or: [ModelScheduleFilterInput]
  not: ModelScheduleFilterInput
}

input CreateScheduleInput {
  id: ID
  day: Int
  endTime: String
  month: Int
  startTime: String
  year: Int
  userScheduleId: ID
  scheduleUserId: ID
}

input UpdateScheduleInput {
  id: ID!
  day: Int
  endTime: String
  month: Int
  startTime: String
  year: Int
  userScheduleId: ID
  scheduleUserId: ID
}

input DeleteScheduleInput {
  id: ID
}

input ModelScheduleConditionInput {
  day: ModelIntInput
  endTime: ModelStringInput
  month: ModelIntInput
  startTime: ModelStringInput
  year: ModelIntInput
  and: [ModelScheduleConditionInput]
  or: [ModelScheduleConditionInput]
  not: ModelScheduleConditionInput
}

type ModelNewsConnection {
  items: [News]
  nextToken: String
}

input ModelNewsFilterInput {
  newsid: ModelIDInput
  Date: ModelStringInput
  Content: ModelStringInput
  and: [ModelNewsFilterInput]
  or: [ModelNewsFilterInput]
  not: ModelNewsFilterInput
}

input CreateNewsInput {
  id: ID
  newsid: ID!
  Date: String
  Content: String
}

input UpdateNewsInput {
  newsid: ID
  Date: String
  Content: String
}

input DeleteNewsInput {
  id: ID
}

input ModelNewsConditionInput {
  newsid: ModelIDInput
  Date: ModelStringInput
  Content: ModelStringInput
  and: [ModelNewsConditionInput]
  or: [ModelNewsConditionInput]
  not: ModelNewsConditionInput
}

type ModelComplaintConnection {
  items: [Complaint]
  nextToken: String
}

input ModelComplaintFilterInput {
  complaintId: ModelIDInput
  Date: ModelStringInput
  content: ModelStringInput
  and: [ModelComplaintFilterInput]
  or: [ModelComplaintFilterInput]
  not: ModelComplaintFilterInput
}

input CreateComplaintInput {
  id: ID
  complaintId: ID!
  Date: String
  content: String
  userComplaintId: ID
  complaintUserId: ID
}

input UpdateComplaintInput {
  complaintId: ID
  Date: String
  content: String
  userComplaintId: ID
  complaintUserId: ID
}

input DeleteComplaintInput {
  id: ID
}

input ModelComplaintConditionInput {
  complaintId: ModelIDInput
  Date: ModelStringInput
  content: ModelStringInput
  and: [ModelComplaintConditionInput]
  or: [ModelComplaintConditionInput]
  not: ModelComplaintConditionInput
}

type ModelFeedbackConnection {
  items: [Feedback]
  nextToken: String
}

input ModelFeedbackFilterInput {
  feedbackId: ModelIDInput
  Date: ModelStringInput
  content: ModelStringInput
  and: [ModelFeedbackFilterInput]
  or: [ModelFeedbackFilterInput]
  not: ModelFeedbackFilterInput
}

input CreateFeedbackInput {
  id: ID
  feedbackId: ID!
  Date: String
  content: String
}

input UpdateFeedbackInput {
  feedbackId: ID
  Date: String
  content: String
}

input DeleteFeedbackInput {
  id: ID
}

input ModelFeedbackConditionInput {
  feedbackId: ModelIDInput
  Date: ModelStringInput
  content: ModelStringInput
  and: [ModelFeedbackConditionInput]
  or: [ModelFeedbackConditionInput]
  not: ModelFeedbackConditionInput
}
