import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-update-password',
  templateUrl: './update-password.component.html',
  styleUrls: ['./update-password.component.scss'
]
})
export class UpdatePasswordComponent  implements OnInit {

  sentCode=false;

  resetForm: FormGroup = new FormGroup({
    username: new FormControl('', [Validators.required]),
    code: new FormControl('', [Validators.required]),
    newPassword: new FormControl('',[Validators.required])
  });

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  sendCode(){
    this.sentCode = true;
  }

  onCancel(){
    this.router.navigate(['/profile']);
  }

  logOut(){
    this.router.navigate(['/login']);
  }

  onHome(){
    this.router.navigate(['/dashboard']);
  }

}
