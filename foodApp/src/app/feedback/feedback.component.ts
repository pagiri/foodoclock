import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { APIService } from '../API.service';
import { API } from 'aws-amplify';
import {formatDate} from '@angular/common';

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.scss']
})


export class FeedbackComponent implements OnInit {


  feedbackForm: FormGroup;
  complaintForm: FormGroup;
  selectedShift: any[];
  msg: any;
  submitted = false; 
  shiftDate: any;
  public shifts: Array<any>;
  selectedStudent: any[];
  public students: Array<any>;
  public isAdminOrSupervisor: boolean = false;
  

  constructor(private route: ActivatedRoute, private router: Router, private formBuilder: FormBuilder, private api : APIService,) {}
  
   async createFeedbackForm() {    
    this.feedbackForm = this.formBuilder.group({
      shiftDate: ['', Validators.required],
      shifts: ['', Validators.required],
      students: ['', Validators.required],
      msg: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(200)]]
      
    });
    //const day = formatDate(this.shiftDate,'dd','en');
    //const month = formatDate(this.shiftDate,'MM','en');
    //const startTime = this.shifts.values
    //const userIds = await this.api.GetSchedule()
    //const feedbackContent = await this.api.CreateComplaint({Date: "05/22/2021", content: "need to change my permanent shift timings", complaintId: "2"}) 
    //console.log(shifts)
    this.feedbackForm.controls["shifts"].setValue(this.selectedShift);
    this.feedbackForm.controls["students"].setValue(this.selectedStudent);
  }

  //  this.shifts = [
  //    { id: 0, value: 'Select Shift' },
  //    { id: 1, value: '11AM - 1PM' },
  //    { id: 2, value: '1PM - 3 PM' },
  //    { id: 3, value: '1PM - 4.30PM' }
  //  ];
  // this.selectedShift = this.shifts[0].id;
  // this.students = [
  //   { id: 0, value: 'Select Student' },
  //   { id: 1, value: 'Priyanka' },
  //   { id: 2, value: 'Pallavi' },
  //   { id: 3, value: 'Hemaleka' },
  //   { id: 4, value: 'Nirutha' }
  // ];
  // this.selectedStudent = this.students[0].id;


  async createComplaintForm() {    
    console.log("aaa");
    this.complaintForm = this.formBuilder.group({

      msg: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(200)]]
     
    });
    
    //const data = {Content:this.msg,Date:formatDate(new Date(), 'yyyy/MM/dd', 'en'), complaintId:"3"};
    //const data = content:'this is test'};
    //const content = await this.api.ListComplaints()
    //const content = await this.api.CreateComplaint({Date: "05/22/2021", content: "need to change my permanent shift timings", complaintId: "2"}) 
    //console.log(content);
  }


  sendFeedback() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.feedbackForm.invalid) {
      return;
    }
    else{
      this.msg = 'Your feedback is submitted successfully';
      console.log(this.feedbackForm.value);
    }

  }

sendComplaint() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.complaintForm.invalid) {
      return;
    }
    else{
      this.msg = 'Your complaint is submitted successfully';
      console.log(this.complaintForm.value);
    }

  }


  onHome(){
    this.router.navigate(['/dashboard']);
  }
  

 async ngOnInit() {


    
      this.createFeedbackForm(); 
      this.createComplaintForm();  
      const content = await this.api.ListFeedbacks()
    //const value = 'I am not very comfortable working under my supervisor'
    //const data = {content:value,Date:formatDate(new Date(), 'yyyy/MM/dd', 'en')};
    //const content = await this.api.CreateComplaint(data)
    //console.log(content);
    //const shifts = await this.api.ListFeedbacks()
    console.log(content);
   
    
  
      
  }
    
  

  
  logOut(){
    this.router.navigate(['/login']);
  }

}
