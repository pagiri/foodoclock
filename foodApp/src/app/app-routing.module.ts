import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ForgotPasswordComponent } from './components/forgot-password/forgot-password.component';
import { LoginComponent } from './components/login/login.component';
import { NewsComponent } from './components/news/news.component';
import { ProfileComponent } from './components/profile/profile.component';
import { ScheduleComponent } from './components/schedule/schedule.component';
import { ConfirmSignUpComponent } from './components/sign-up/confirm-sign-up/confirm-sign-up.component';
import { SignUpComponent } from './components/sign-up/sign-up.component';
import { FeedbackComponent } from './feedback/feedback.component';
import { UpdatePasswordComponent } from './update-password/update-password.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'login' },
  { path: 'login', component: LoginComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'profile', component: ProfileComponent },
  { path: 'schedule', component: ScheduleComponent },
  { path: 'news', component: NewsComponent },
  {path:'forgotPassword', component:ForgotPasswordComponent},
  {path:'signUp', component: SignUpComponent},
  {path:'update-password', component:UpdatePasswordComponent},
  {path:'feedback', component:FeedbackComponent},
  {path:'confirmSignUp', component:ConfirmSignUpComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
