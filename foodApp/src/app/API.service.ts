/* tslint:disable */
/* eslint-disable */
//  This file was automatically generated and should not be edited.
import { Injectable } from "@angular/core";
import API, { graphqlOperation, GraphQLResult } from "@aws-amplify/api-graphql";
import { Observable } from "zen-observable-ts";

export interface SubscriptionResponse<T> {
  value: GraphQLResult<T>;
}

export type CreateUserInput = {
  id?: string | null;
  userId?: string | null;
  FirstName: string;
  LastName?: string | null;
  SUID: number;
  PhoneNumber?: number | null;
  username?: string | null;
  Email: string;
  Role?: string | null;
};

export type ModelUserConditionInput = {
  userId?: ModelIDInput | null;
  FirstName?: ModelStringInput | null;
  LastName?: ModelStringInput | null;
  SUID?: ModelIntInput | null;
  PhoneNumber?: ModelIntInput | null;
  username?: ModelStringInput | null;
  Email?: ModelStringInput | null;
  Role?: ModelStringInput | null;
  and?: Array<ModelUserConditionInput | null> | null;
  or?: Array<ModelUserConditionInput | null> | null;
  not?: ModelUserConditionInput | null;
};

export type ModelIDInput = {
  ne?: string | null;
  eq?: string | null;
  le?: string | null;
  lt?: string | null;
  ge?: string | null;
  gt?: string | null;
  contains?: string | null;
  notContains?: string | null;
  between?: Array<string | null> | null;
  beginsWith?: string | null;
  attributeExists?: boolean | null;
  attributeType?: ModelAttributeTypes | null;
  size?: ModelSizeInput | null;
};

export enum ModelAttributeTypes {
  binary = "binary",
  binarySet = "binarySet",
  bool = "bool",
  list = "list",
  map = "map",
  number = "number",
  numberSet = "numberSet",
  string = "string",
  stringSet = "stringSet",
  _null = "_null"
}

export type ModelSizeInput = {
  ne?: number | null;
  eq?: number | null;
  le?: number | null;
  lt?: number | null;
  ge?: number | null;
  gt?: number | null;
  between?: Array<number | null> | null;
};

export type ModelStringInput = {
  ne?: string | null;
  eq?: string | null;
  le?: string | null;
  lt?: string | null;
  ge?: string | null;
  gt?: string | null;
  contains?: string | null;
  notContains?: string | null;
  between?: Array<string | null> | null;
  beginsWith?: string | null;
  attributeExists?: boolean | null;
  attributeType?: ModelAttributeTypes | null;
  size?: ModelSizeInput | null;
};

export type ModelIntInput = {
  ne?: number | null;
  eq?: number | null;
  le?: number | null;
  lt?: number | null;
  ge?: number | null;
  gt?: number | null;
  between?: Array<number | null> | null;
  attributeExists?: boolean | null;
  attributeType?: ModelAttributeTypes | null;
};

export type User = {
  __typename: "User";
  id?: string;
  userId?: string | null;
  FirstName?: string;
  LastName?: string | null;
  SUID?: number;
  PhoneNumber?: number | null;
  username?: string | null;
  Email?: string;
  Role?: string | null;
  schedule?: ModelScheduleConnection;
  complaint?: ModelComplaintConnection;
  feedback?: ModelFeedbackConnection;
  createdAt?: string;
  updatedAt?: string;
};

export type ModelScheduleConnection = {
  __typename: "ModelScheduleConnection";
  items?: Array<Schedule | null> | null;
  nextToken?: string | null;
};

export type Schedule = {
  __typename: "Schedule";
  id?: string;
  day?: number | null;
  endTime?: string | null;
  month?: number | null;
  startTime?: string | null;
  year?: number | null;
  user?: User;
  createdAt?: string;
  updatedAt?: string;
};

export type ModelComplaintConnection = {
  __typename: "ModelComplaintConnection";
  items?: Array<Complaint | null> | null;
  nextToken?: string | null;
};

export type Complaint = {
  __typename: "Complaint";
  id?: string;
  complaintId?: string | null;
  Date?: string | null;
  content?: string | null;
  user?: User;
  createdAt?: string;
  updatedAt?: string;
};

export type ModelFeedbackConnection = {
  __typename: "ModelFeedbackConnection";
  items?: Array<Feedback | null> | null;
  nextToken?: string | null;
};

export type Feedback = {
  __typename: "Feedback";
  id?: string;
  feedbackId?: string | null;
  Date?: string | null;
  content?: string | null;
  user?: User;
  createdAt?: string;
  updatedAt?: string;
};

export type UpdateUserInput = {
  userId?: string | null;
  FirstName?: string | null;
  LastName?: string | null;
  SUID?: number | null;
  PhoneNumber?: number | null;
  username?: string | null;
  Email?: string | null;
  Role?: string | null;
};

export type DeleteUserInput = {
  id?: string | null;
};

export type CreateScheduleInput = {
  id?: string | null;
  day?: number | null;
  endTime?: string | null;
  month?: number | null;
  startTime?: string | null;
  year?: number | null;
  userScheduleId?: string | null;
  scheduleUserId?: string | null;
};

export type ModelScheduleConditionInput = {
  day?: ModelIntInput | null;
  endTime?: ModelStringInput | null;
  month?: ModelIntInput | null;
  startTime?: ModelStringInput | null;
  year?: ModelIntInput | null;
  and?: Array<ModelScheduleConditionInput | null> | null;
  or?: Array<ModelScheduleConditionInput | null> | null;
  not?: ModelScheduleConditionInput | null;
};

export type UpdateScheduleInput = {
  id: string;
  day?: number | null;
  endTime?: string | null;
  month?: number | null;
  startTime?: string | null;
  year?: number | null;
  userScheduleId?: string | null;
  scheduleUserId?: string | null;
};

export type DeleteScheduleInput = {
  id?: string | null;
};

export type CreateNewsInput = {
  id?: string | null;
  newsid?: string | null;
  Date?: string | null;
  Content?: string | null;
};

export type ModelNewsConditionInput = {
  newsid?: ModelIDInput | null;
  Date?: ModelStringInput | null;
  Content?: ModelStringInput | null;
  and?: Array<ModelNewsConditionInput | null> | null;
  or?: Array<ModelNewsConditionInput | null> | null;
  not?: ModelNewsConditionInput | null;
};

export type News = {
  __typename: "News";
  id?: string;
  newsid?: string | null;
  Date?: string | null;
  Content?: string | null;
  createdAt?: string;
  updatedAt?: string;
};

export type UpdateNewsInput = {
  newsid?: string | null;
  Date?: string | null;
  Content?: string | null;
};

export type DeleteNewsInput = {
  id?: string | null;
};

export type CreateComplaintInput = {
  id?: string | null;
  complaintId?: string | null;
  Date?: string | null;
  content?: string | null;
  userComplaintId?: string | null;
  complaintUserId?: string | null;
};

export type ModelComplaintConditionInput = {
  complaintId?: ModelIDInput | null;
  Date?: ModelStringInput | null;
  content?: ModelStringInput | null;
  and?: Array<ModelComplaintConditionInput | null> | null;
  or?: Array<ModelComplaintConditionInput | null> | null;
  not?: ModelComplaintConditionInput | null;
};

export type UpdateComplaintInput = {
  complaintId?: string | null;
  Date?: string | null;
  content?: string | null;
  userComplaintId?: string | null;
  complaintUserId?: string | null;
};

export type DeleteComplaintInput = {
  id?: string | null;
};

export type CreateFeedbackInput = {
  id?: string | null;
  feedbackId?: string | null;
  Date?: string | null;
  content?: string | null;
  userFeedbackId?: string | null;
  feedbackUserId?: string | null;
};

export type ModelFeedbackConditionInput = {
  feedbackId?: ModelIDInput | null;
  Date?: ModelStringInput | null;
  content?: ModelStringInput | null;
  and?: Array<ModelFeedbackConditionInput | null> | null;
  or?: Array<ModelFeedbackConditionInput | null> | null;
  not?: ModelFeedbackConditionInput | null;
};

export type UpdateFeedbackInput = {
  feedbackId?: string | null;
  Date?: string | null;
  content?: string | null;
  userFeedbackId?: string | null;
  feedbackUserId?: string | null;
};

export type DeleteFeedbackInput = {
  id?: string | null;
};

export type ModelUserFilterInput = {
  userId?: ModelIDInput | null;
  FirstName?: ModelStringInput | null;
  LastName?: ModelStringInput | null;
  SUID?: ModelIntInput | null;
  PhoneNumber?: ModelIntInput | null;
  username?: ModelStringInput | null;
  Email?: ModelStringInput | null;
  Role?: ModelStringInput | null;
  and?: Array<ModelUserFilterInput | null> | null;
  or?: Array<ModelUserFilterInput | null> | null;
  not?: ModelUserFilterInput | null;
};

export type ModelUserConnection = {
  __typename: "ModelUserConnection";
  items?: Array<User | null> | null;
  nextToken?: string | null;
};

export type ModelScheduleFilterInput = {
  id?: ModelIDInput | null;
  day?: ModelIntInput | null;
  endTime?: ModelStringInput | null;
  month?: ModelIntInput | null;
  startTime?: ModelStringInput | null;
  year?: ModelIntInput | null;
  and?: Array<ModelScheduleFilterInput | null> | null;
  or?: Array<ModelScheduleFilterInput | null> | null;
  not?: ModelScheduleFilterInput | null;
};

export type ModelNewsFilterInput = {
  newsid?: ModelIDInput | null;
  Date?: ModelStringInput | null;
  Content?: ModelStringInput | null;
  and?: Array<ModelNewsFilterInput | null> | null;
  or?: Array<ModelNewsFilterInput | null> | null;
  not?: ModelNewsFilterInput | null;
};

export type ModelNewsConnection = {
  __typename: "ModelNewsConnection";
  items?: Array<News | null> | null;
  nextToken?: string | null;
};

export type ModelComplaintFilterInput = {
  complaintId?: ModelIDInput | null;
  Date?: ModelStringInput | null;
  content?: ModelStringInput | null;
  and?: Array<ModelComplaintFilterInput | null> | null;
  or?: Array<ModelComplaintFilterInput | null> | null;
  not?: ModelComplaintFilterInput | null;
};

export type ModelFeedbackFilterInput = {
  feedbackId?: ModelIDInput | null;
  Date?: ModelStringInput | null;
  content?: ModelStringInput | null;
  and?: Array<ModelFeedbackFilterInput | null> | null;
  or?: Array<ModelFeedbackFilterInput | null> | null;
  not?: ModelFeedbackFilterInput | null;
};

export type CreateUserMutation = {
  __typename: "User";
  id: string;
  userId?: string | null;
  FirstName: string;
  LastName?: string | null;
  SUID: number;
  PhoneNumber?: number | null;
  username?: string | null;
  Email: string;
  Role?: string | null;
  schedule?: {
    __typename: "ModelScheduleConnection";
    items?: Array<{
      __typename: "Schedule";
      id: string;
      day?: number | null;
      endTime?: string | null;
      month?: number | null;
      startTime?: string | null;
      year?: number | null;
      createdAt: string;
      updatedAt: string;
    } | null> | null;
    nextToken?: string | null;
  } | null;
  complaint?: {
    __typename: "ModelComplaintConnection";
    items?: Array<{
      __typename: "Complaint";
      id: string;
      complaintId?: string | null;
      Date?: string | null;
      content?: string | null;
      createdAt: string;
      updatedAt: string;
    } | null> | null;
    nextToken?: string | null;
  } | null;
  feedback?: {
    __typename: "ModelFeedbackConnection";
    items?: Array<{
      __typename: "Feedback";
      id: string;
      feedbackId?: string | null;
      Date?: string | null;
      content?: string | null;
      createdAt: string;
      updatedAt: string;
    } | null> | null;
    nextToken?: string | null;
  } | null;
  createdAt: string;
  updatedAt: string;
};

export type UpdateUserMutation = {
  __typename: "User";
  id: string;
  userId?: string | null;
  FirstName: string;
  LastName?: string | null;
  SUID: number;
  PhoneNumber?: number | null;
  username?: string | null;
  Email: string;
  Role?: string | null;
  schedule?: {
    __typename: "ModelScheduleConnection";
    items?: Array<{
      __typename: "Schedule";
      id: string;
      day?: number | null;
      endTime?: string | null;
      month?: number | null;
      startTime?: string | null;
      year?: number | null;
      createdAt: string;
      updatedAt: string;
    } | null> | null;
    nextToken?: string | null;
  } | null;
  complaint?: {
    __typename: "ModelComplaintConnection";
    items?: Array<{
      __typename: "Complaint";
      id: string;
      complaintId?: string | null;
      Date?: string | null;
      content?: string | null;
      createdAt: string;
      updatedAt: string;
    } | null> | null;
    nextToken?: string | null;
  } | null;
  feedback?: {
    __typename: "ModelFeedbackConnection";
    items?: Array<{
      __typename: "Feedback";
      id: string;
      feedbackId?: string | null;
      Date?: string | null;
      content?: string | null;
      createdAt: string;
      updatedAt: string;
    } | null> | null;
    nextToken?: string | null;
  } | null;
  createdAt: string;
  updatedAt: string;
};

export type DeleteUserMutation = {
  __typename: "User";
  id: string;
  userId?: string | null;
  FirstName: string;
  LastName?: string | null;
  SUID: number;
  PhoneNumber?: number | null;
  username?: string | null;
  Email: string;
  Role?: string | null;
  schedule?: {
    __typename: "ModelScheduleConnection";
    items?: Array<{
      __typename: "Schedule";
      id: string;
      day?: number | null;
      endTime?: string | null;
      month?: number | null;
      startTime?: string | null;
      year?: number | null;
      createdAt: string;
      updatedAt: string;
    } | null> | null;
    nextToken?: string | null;
  } | null;
  complaint?: {
    __typename: "ModelComplaintConnection";
    items?: Array<{
      __typename: "Complaint";
      id: string;
      complaintId?: string | null;
      Date?: string | null;
      content?: string | null;
      createdAt: string;
      updatedAt: string;
    } | null> | null;
    nextToken?: string | null;
  } | null;
  feedback?: {
    __typename: "ModelFeedbackConnection";
    items?: Array<{
      __typename: "Feedback";
      id: string;
      feedbackId?: string | null;
      Date?: string | null;
      content?: string | null;
      createdAt: string;
      updatedAt: string;
    } | null> | null;
    nextToken?: string | null;
  } | null;
  createdAt: string;
  updatedAt: string;
};

export type CreateScheduleMutation = {
  __typename: "Schedule";
  id: string;
  day?: number | null;
  endTime?: string | null;
  month?: number | null;
  startTime?: string | null;
  year?: number | null;
  user?: {
    __typename: "User";
    id: string;
    userId?: string | null;
    FirstName: string;
    LastName?: string | null;
    SUID: number;
    PhoneNumber?: number | null;
    username?: string | null;
    Email: string;
    Role?: string | null;
    schedule?: {
      __typename: "ModelScheduleConnection";
      nextToken?: string | null;
    } | null;
    complaint?: {
      __typename: "ModelComplaintConnection";
      nextToken?: string | null;
    } | null;
    feedback?: {
      __typename: "ModelFeedbackConnection";
      nextToken?: string | null;
    } | null;
    createdAt: string;
    updatedAt: string;
  } | null;
  createdAt: string;
  updatedAt: string;
};

export type UpdateScheduleMutation = {
  __typename: "Schedule";
  id: string;
  day?: number | null;
  endTime?: string | null;
  month?: number | null;
  startTime?: string | null;
  year?: number | null;
  user?: {
    __typename: "User";
    id: string;
    userId?: string | null;
    FirstName: string;
    LastName?: string | null;
    SUID: number;
    PhoneNumber?: number | null;
    username?: string | null;
    Email: string;
    Role?: string | null;
    schedule?: {
      __typename: "ModelScheduleConnection";
      nextToken?: string | null;
    } | null;
    complaint?: {
      __typename: "ModelComplaintConnection";
      nextToken?: string | null;
    } | null;
    feedback?: {
      __typename: "ModelFeedbackConnection";
      nextToken?: string | null;
    } | null;
    createdAt: string;
    updatedAt: string;
  } | null;
  createdAt: string;
  updatedAt: string;
};

export type DeleteScheduleMutation = {
  __typename: "Schedule";
  id: string;
  day?: number | null;
  endTime?: string | null;
  month?: number | null;
  startTime?: string | null;
  year?: number | null;
  user?: {
    __typename: "User";
    id: string;
    userId?: string | null;
    FirstName: string;
    LastName?: string | null;
    SUID: number;
    PhoneNumber?: number | null;
    username?: string | null;
    Email: string;
    Role?: string | null;
    schedule?: {
      __typename: "ModelScheduleConnection";
      nextToken?: string | null;
    } | null;
    complaint?: {
      __typename: "ModelComplaintConnection";
      nextToken?: string | null;
    } | null;
    feedback?: {
      __typename: "ModelFeedbackConnection";
      nextToken?: string | null;
    } | null;
    createdAt: string;
    updatedAt: string;
  } | null;
  createdAt: string;
  updatedAt: string;
};

export type CreateNewsMutation = {
  __typename: "News";
  id: string;
  newsid?: string | null;
  Date?: string | null;
  Content?: string | null;
  createdAt: string;
  updatedAt: string;
};

export type UpdateNewsMutation = {
  __typename: "News";
  id: string;
  newsid?: string | null;
  Date?: string | null;
  Content?: string | null;
  createdAt: string;
  updatedAt: string;
};

export type DeleteNewsMutation = {
  __typename: "News";
  id: string;
  newsid?: string | null;
  Date?: string | null;
  Content?: string | null;
  createdAt: string;
  updatedAt: string;
};

export type CreateComplaintMutation = {
  __typename: "Complaint";
  id: string;
  complaintId?: string | null;
  Date?: string | null;
  content?: string | null;
  user?: {
    __typename: "User";
    id: string;
    userId?: string | null;
    FirstName: string;
    LastName?: string | null;
    SUID: number;
    PhoneNumber?: number | null;
    username?: string | null;
    Email: string;
    Role?: string | null;
    schedule?: {
      __typename: "ModelScheduleConnection";
      nextToken?: string | null;
    } | null;
    complaint?: {
      __typename: "ModelComplaintConnection";
      nextToken?: string | null;
    } | null;
    feedback?: {
      __typename: "ModelFeedbackConnection";
      nextToken?: string | null;
    } | null;
    createdAt: string;
    updatedAt: string;
  } | null;
  createdAt: string;
  updatedAt: string;
};

export type UpdateComplaintMutation = {
  __typename: "Complaint";
  id: string;
  complaintId?: string | null;
  Date?: string | null;
  content?: string | null;
  user?: {
    __typename: "User";
    id: string;
    userId?: string | null;
    FirstName: string;
    LastName?: string | null;
    SUID: number;
    PhoneNumber?: number | null;
    username?: string | null;
    Email: string;
    Role?: string | null;
    schedule?: {
      __typename: "ModelScheduleConnection";
      nextToken?: string | null;
    } | null;
    complaint?: {
      __typename: "ModelComplaintConnection";
      nextToken?: string | null;
    } | null;
    feedback?: {
      __typename: "ModelFeedbackConnection";
      nextToken?: string | null;
    } | null;
    createdAt: string;
    updatedAt: string;
  } | null;
  createdAt: string;
  updatedAt: string;
};

export type DeleteComplaintMutation = {
  __typename: "Complaint";
  id: string;
  complaintId?: string | null;
  Date?: string | null;
  content?: string | null;
  user?: {
    __typename: "User";
    id: string;
    userId?: string | null;
    FirstName: string;
    LastName?: string | null;
    SUID: number;
    PhoneNumber?: number | null;
    username?: string | null;
    Email: string;
    Role?: string | null;
    schedule?: {
      __typename: "ModelScheduleConnection";
      nextToken?: string | null;
    } | null;
    complaint?: {
      __typename: "ModelComplaintConnection";
      nextToken?: string | null;
    } | null;
    feedback?: {
      __typename: "ModelFeedbackConnection";
      nextToken?: string | null;
    } | null;
    createdAt: string;
    updatedAt: string;
  } | null;
  createdAt: string;
  updatedAt: string;
};

export type CreateFeedbackMutation = {
  __typename: "Feedback";
  id: string;
  feedbackId?: string | null;
  Date?: string | null;
  content?: string | null;
  user?: {
    __typename: "User";
    id: string;
    userId?: string | null;
    FirstName: string;
    LastName?: string | null;
    SUID: number;
    PhoneNumber?: number | null;
    username?: string | null;
    Email: string;
    Role?: string | null;
    schedule?: {
      __typename: "ModelScheduleConnection";
      nextToken?: string | null;
    } | null;
    complaint?: {
      __typename: "ModelComplaintConnection";
      nextToken?: string | null;
    } | null;
    feedback?: {
      __typename: "ModelFeedbackConnection";
      nextToken?: string | null;
    } | null;
    createdAt: string;
    updatedAt: string;
  } | null;
  createdAt: string;
  updatedAt: string;
};

export type UpdateFeedbackMutation = {
  __typename: "Feedback";
  id: string;
  feedbackId?: string | null;
  Date?: string | null;
  content?: string | null;
  user?: {
    __typename: "User";
    id: string;
    userId?: string | null;
    FirstName: string;
    LastName?: string | null;
    SUID: number;
    PhoneNumber?: number | null;
    username?: string | null;
    Email: string;
    Role?: string | null;
    schedule?: {
      __typename: "ModelScheduleConnection";
      nextToken?: string | null;
    } | null;
    complaint?: {
      __typename: "ModelComplaintConnection";
      nextToken?: string | null;
    } | null;
    feedback?: {
      __typename: "ModelFeedbackConnection";
      nextToken?: string | null;
    } | null;
    createdAt: string;
    updatedAt: string;
  } | null;
  createdAt: string;
  updatedAt: string;
};

export type DeleteFeedbackMutation = {
  __typename: "Feedback";
  id: string;
  feedbackId?: string | null;
  Date?: string | null;
  content?: string | null;
  user?: {
    __typename: "User";
    id: string;
    userId?: string | null;
    FirstName: string;
    LastName?: string | null;
    SUID: number;
    PhoneNumber?: number | null;
    username?: string | null;
    Email: string;
    Role?: string | null;
    schedule?: {
      __typename: "ModelScheduleConnection";
      nextToken?: string | null;
    } | null;
    complaint?: {
      __typename: "ModelComplaintConnection";
      nextToken?: string | null;
    } | null;
    feedback?: {
      __typename: "ModelFeedbackConnection";
      nextToken?: string | null;
    } | null;
    createdAt: string;
    updatedAt: string;
  } | null;
  createdAt: string;
  updatedAt: string;
};

export type GetUserQuery = {
  __typename: "User";
  id: string;
  userId?: string | null;
  FirstName: string;
  LastName?: string | null;
  SUID: number;
  PhoneNumber?: number | null;
  username?: string | null;
  Email: string;
  Role?: string | null;
  schedule?: {
    __typename: "ModelScheduleConnection";
    items?: Array<{
      __typename: "Schedule";
      id: string;
      day?: number | null;
      endTime?: string | null;
      month?: number | null;
      startTime?: string | null;
      year?: number | null;
      createdAt: string;
      updatedAt: string;
    } | null> | null;
    nextToken?: string | null;
  } | null;
  complaint?: {
    __typename: "ModelComplaintConnection";
    items?: Array<{
      __typename: "Complaint";
      id: string;
      complaintId?: string | null;
      Date?: string | null;
      content?: string | null;
      createdAt: string;
      updatedAt: string;
    } | null> | null;
    nextToken?: string | null;
  } | null;
  feedback?: {
    __typename: "ModelFeedbackConnection";
    items?: Array<{
      __typename: "Feedback";
      id: string;
      feedbackId?: string | null;
      Date?: string | null;
      content?: string | null;
      createdAt: string;
      updatedAt: string;
    } | null> | null;
    nextToken?: string | null;
  } | null;
  createdAt: string;
  updatedAt: string;
};

export type ListUsersQuery = {
  __typename: "ModelUserConnection";
  items?: Array<{
    __typename: "User";
    id: string;
    userId?: string | null;
    FirstName: string;
    LastName?: string | null;
    SUID: number;
    PhoneNumber?: number | null;
    username?: string | null;
    Email: string;
    Role?: string | null;
    schedule?: {
      __typename: "ModelScheduleConnection";
      nextToken?: string | null;
    } | null;
    complaint?: {
      __typename: "ModelComplaintConnection";
      nextToken?: string | null;
    } | null;
    feedback?: {
      __typename: "ModelFeedbackConnection";
      nextToken?: string | null;
    } | null;
    createdAt: string;
    updatedAt: string;
  } | null> | null;
  nextToken?: string | null;
};

export type GetScheduleQuery = {
  __typename: "Schedule";
  id: string;
  day?: number | null;
  endTime?: string | null;
  month?: number | null;
  startTime?: string | null;
  year?: number | null;
  user?: {
    __typename: "User";
    id: string;
    userId?: string | null;
    FirstName: string;
    LastName?: string | null;
    SUID: number;
    PhoneNumber?: number | null;
    username?: string | null;
    Email: string;
    Role?: string | null;
    schedule?: {
      __typename: "ModelScheduleConnection";
      nextToken?: string | null;
    } | null;
    complaint?: {
      __typename: "ModelComplaintConnection";
      nextToken?: string | null;
    } | null;
    feedback?: {
      __typename: "ModelFeedbackConnection";
      nextToken?: string | null;
    } | null;
    createdAt: string;
    updatedAt: string;
  } | null;
  createdAt: string;
  updatedAt: string;
};

export type ListSchedulesQuery = {
  __typename: "ModelScheduleConnection";
  items?: Array<{
    __typename: "Schedule";
    id: string;
    day?: number | null;
    endTime?: string | null;
    month?: number | null;
    startTime?: string | null;
    year?: number | null;
    user?: {
      __typename: "User";
      id: string;
      userId?: string | null;
      FirstName: string;
      LastName?: string | null;
      SUID: number;
      PhoneNumber?: number | null;
      username?: string | null;
      Email: string;
      Role?: string | null;
      createdAt: string;
      updatedAt: string;
    } | null;
    createdAt: string;
    updatedAt: string;
  } | null> | null;
  nextToken?: string | null;
};

export type GetNewsQuery = {
  __typename: "News";
  id: string;
  newsid?: string | null;
  Date?: string | null;
  Content?: string | null;
  createdAt: string;
  updatedAt: string;
};

export type ListNewssQuery = {
  __typename: "ModelNewsConnection";
  items?: Array<{
    __typename: "News";
    id: string;
    newsid?: string | null;
    Date?: string | null;
    Content?: string | null;
    createdAt: string;
    updatedAt: string;
  } | null> | null;
  nextToken?: string | null;
};

export type GetComplaintQuery = {
  __typename: "Complaint";
  id: string;
  complaintId?: string | null;
  Date?: string | null;
  content?: string | null;
  user?: {
    __typename: "User";
    id: string;
    userId?: string | null;
    FirstName: string;
    LastName?: string | null;
    SUID: number;
    PhoneNumber?: number | null;
    username?: string | null;
    Email: string;
    Role?: string | null;
    schedule?: {
      __typename: "ModelScheduleConnection";
      nextToken?: string | null;
    } | null;
    complaint?: {
      __typename: "ModelComplaintConnection";
      nextToken?: string | null;
    } | null;
    feedback?: {
      __typename: "ModelFeedbackConnection";
      nextToken?: string | null;
    } | null;
    createdAt: string;
    updatedAt: string;
  } | null;
  createdAt: string;
  updatedAt: string;
};

export type ListComplaintsQuery = {
  __typename: "ModelComplaintConnection";
  items?: Array<{
    __typename: "Complaint";
    id: string;
    complaintId?: string | null;
    Date?: string | null;
    content?: string | null;
    user?: {
      __typename: "User";
      id: string;
      userId?: string | null;
      FirstName: string;
      LastName?: string | null;
      SUID: number;
      PhoneNumber?: number | null;
      username?: string | null;
      Email: string;
      Role?: string | null;
      createdAt: string;
      updatedAt: string;
    } | null;
    createdAt: string;
    updatedAt: string;
  } | null> | null;
  nextToken?: string | null;
};

export type GetFeedbackQuery = {
  __typename: "Feedback";
  id: string;
  feedbackId?: string | null;
  Date?: string | null;
  content?: string | null;
  user?: {
    __typename: "User";
    id: string;
    userId?: string | null;
    FirstName: string;
    LastName?: string | null;
    SUID: number;
    PhoneNumber?: number | null;
    username?: string | null;
    Email: string;
    Role?: string | null;
    schedule?: {
      __typename: "ModelScheduleConnection";
      nextToken?: string | null;
    } | null;
    complaint?: {
      __typename: "ModelComplaintConnection";
      nextToken?: string | null;
    } | null;
    feedback?: {
      __typename: "ModelFeedbackConnection";
      nextToken?: string | null;
    } | null;
    createdAt: string;
    updatedAt: string;
  } | null;
  createdAt: string;
  updatedAt: string;
};

export type ListFeedbacksQuery = {
  __typename: "ModelFeedbackConnection";
  items?: Array<{
    __typename: "Feedback";
    id: string;
    feedbackId?: string | null;
    Date?: string | null;
    content?: string | null;
    user?: {
      __typename: "User";
      id: string;
      userId?: string | null;
      FirstName: string;
      LastName?: string | null;
      SUID: number;
      PhoneNumber?: number | null;
      username?: string | null;
      Email: string;
      Role?: string | null;
      createdAt: string;
      updatedAt: string;
    } | null;
    createdAt: string;
    updatedAt: string;
  } | null> | null;
  nextToken?: string | null;
};

export type OnCreateUserSubscription = {
  __typename: "User";
  id: string;
  userId?: string | null;
  FirstName: string;
  LastName?: string | null;
  SUID: number;
  PhoneNumber?: number | null;
  username?: string | null;
  Email: string;
  Role?: string | null;
  schedule?: {
    __typename: "ModelScheduleConnection";
    items?: Array<{
      __typename: "Schedule";
      id: string;
      day?: number | null;
      endTime?: string | null;
      month?: number | null;
      startTime?: string | null;
      year?: number | null;
      createdAt: string;
      updatedAt: string;
    } | null> | null;
    nextToken?: string | null;
  } | null;
  complaint?: {
    __typename: "ModelComplaintConnection";
    items?: Array<{
      __typename: "Complaint";
      id: string;
      complaintId?: string | null;
      Date?: string | null;
      content?: string | null;
      createdAt: string;
      updatedAt: string;
    } | null> | null;
    nextToken?: string | null;
  } | null;
  feedback?: {
    __typename: "ModelFeedbackConnection";
    items?: Array<{
      __typename: "Feedback";
      id: string;
      feedbackId?: string | null;
      Date?: string | null;
      content?: string | null;
      createdAt: string;
      updatedAt: string;
    } | null> | null;
    nextToken?: string | null;
  } | null;
  createdAt: string;
  updatedAt: string;
};

export type OnUpdateUserSubscription = {
  __typename: "User";
  id: string;
  userId?: string | null;
  FirstName: string;
  LastName?: string | null;
  SUID: number;
  PhoneNumber?: number | null;
  username?: string | null;
  Email: string;
  Role?: string | null;
  schedule?: {
    __typename: "ModelScheduleConnection";
    items?: Array<{
      __typename: "Schedule";
      id: string;
      day?: number | null;
      endTime?: string | null;
      month?: number | null;
      startTime?: string | null;
      year?: number | null;
      createdAt: string;
      updatedAt: string;
    } | null> | null;
    nextToken?: string | null;
  } | null;
  complaint?: {
    __typename: "ModelComplaintConnection";
    items?: Array<{
      __typename: "Complaint";
      id: string;
      complaintId?: string | null;
      Date?: string | null;
      content?: string | null;
      createdAt: string;
      updatedAt: string;
    } | null> | null;
    nextToken?: string | null;
  } | null;
  feedback?: {
    __typename: "ModelFeedbackConnection";
    items?: Array<{
      __typename: "Feedback";
      id: string;
      feedbackId?: string | null;
      Date?: string | null;
      content?: string | null;
      createdAt: string;
      updatedAt: string;
    } | null> | null;
    nextToken?: string | null;
  } | null;
  createdAt: string;
  updatedAt: string;
};

export type OnDeleteUserSubscription = {
  __typename: "User";
  id: string;
  userId?: string | null;
  FirstName: string;
  LastName?: string | null;
  SUID: number;
  PhoneNumber?: number | null;
  username?: string | null;
  Email: string;
  Role?: string | null;
  schedule?: {
    __typename: "ModelScheduleConnection";
    items?: Array<{
      __typename: "Schedule";
      id: string;
      day?: number | null;
      endTime?: string | null;
      month?: number | null;
      startTime?: string | null;
      year?: number | null;
      createdAt: string;
      updatedAt: string;
    } | null> | null;
    nextToken?: string | null;
  } | null;
  complaint?: {
    __typename: "ModelComplaintConnection";
    items?: Array<{
      __typename: "Complaint";
      id: string;
      complaintId?: string | null;
      Date?: string | null;
      content?: string | null;
      createdAt: string;
      updatedAt: string;
    } | null> | null;
    nextToken?: string | null;
  } | null;
  feedback?: {
    __typename: "ModelFeedbackConnection";
    items?: Array<{
      __typename: "Feedback";
      id: string;
      feedbackId?: string | null;
      Date?: string | null;
      content?: string | null;
      createdAt: string;
      updatedAt: string;
    } | null> | null;
    nextToken?: string | null;
  } | null;
  createdAt: string;
  updatedAt: string;
};

export type OnCreateScheduleSubscription = {
  __typename: "Schedule";
  id: string;
  day?: number | null;
  endTime?: string | null;
  month?: number | null;
  startTime?: string | null;
  year?: number | null;
  user?: {
    __typename: "User";
    id: string;
    userId?: string | null;
    FirstName: string;
    LastName?: string | null;
    SUID: number;
    PhoneNumber?: number | null;
    username?: string | null;
    Email: string;
    Role?: string | null;
    schedule?: {
      __typename: "ModelScheduleConnection";
      nextToken?: string | null;
    } | null;
    complaint?: {
      __typename: "ModelComplaintConnection";
      nextToken?: string | null;
    } | null;
    feedback?: {
      __typename: "ModelFeedbackConnection";
      nextToken?: string | null;
    } | null;
    createdAt: string;
    updatedAt: string;
  } | null;
  createdAt: string;
  updatedAt: string;
};

export type OnUpdateScheduleSubscription = {
  __typename: "Schedule";
  id: string;
  day?: number | null;
  endTime?: string | null;
  month?: number | null;
  startTime?: string | null;
  year?: number | null;
  user?: {
    __typename: "User";
    id: string;
    userId?: string | null;
    FirstName: string;
    LastName?: string | null;
    SUID: number;
    PhoneNumber?: number | null;
    username?: string | null;
    Email: string;
    Role?: string | null;
    schedule?: {
      __typename: "ModelScheduleConnection";
      nextToken?: string | null;
    } | null;
    complaint?: {
      __typename: "ModelComplaintConnection";
      nextToken?: string | null;
    } | null;
    feedback?: {
      __typename: "ModelFeedbackConnection";
      nextToken?: string | null;
    } | null;
    createdAt: string;
    updatedAt: string;
  } | null;
  createdAt: string;
  updatedAt: string;
};

export type OnDeleteScheduleSubscription = {
  __typename: "Schedule";
  id: string;
  day?: number | null;
  endTime?: string | null;
  month?: number | null;
  startTime?: string | null;
  year?: number | null;
  user?: {
    __typename: "User";
    id: string;
    userId?: string | null;
    FirstName: string;
    LastName?: string | null;
    SUID: number;
    PhoneNumber?: number | null;
    username?: string | null;
    Email: string;
    Role?: string | null;
    schedule?: {
      __typename: "ModelScheduleConnection";
      nextToken?: string | null;
    } | null;
    complaint?: {
      __typename: "ModelComplaintConnection";
      nextToken?: string | null;
    } | null;
    feedback?: {
      __typename: "ModelFeedbackConnection";
      nextToken?: string | null;
    } | null;
    createdAt: string;
    updatedAt: string;
  } | null;
  createdAt: string;
  updatedAt: string;
};

export type OnCreateNewsSubscription = {
  __typename: "News";
  id: string;
  newsid?: string | null;
  Date?: string | null;
  Content?: string | null;
  createdAt: string;
  updatedAt: string;
};

export type OnUpdateNewsSubscription = {
  __typename: "News";
  id: string;
  newsid?: string | null;
  Date?: string | null;
  Content?: string | null;
  createdAt: string;
  updatedAt: string;
};

export type OnDeleteNewsSubscription = {
  __typename: "News";
  id: string;
  newsid?: string | null;
  Date?: string | null;
  Content?: string | null;
  createdAt: string;
  updatedAt: string;
};

export type OnCreateComplaintSubscription = {
  __typename: "Complaint";
  id: string;
  complaintId?: string | null;
  Date?: string | null;
  content?: string | null;
  user?: {
    __typename: "User";
    id: string;
    userId?: string | null;
    FirstName: string;
    LastName?: string | null;
    SUID: number;
    PhoneNumber?: number | null;
    username?: string | null;
    Email: string;
    Role?: string | null;
    schedule?: {
      __typename: "ModelScheduleConnection";
      nextToken?: string | null;
    } | null;
    complaint?: {
      __typename: "ModelComplaintConnection";
      nextToken?: string | null;
    } | null;
    feedback?: {
      __typename: "ModelFeedbackConnection";
      nextToken?: string | null;
    } | null;
    createdAt: string;
    updatedAt: string;
  } | null;
  createdAt: string;
  updatedAt: string;
};

export type OnUpdateComplaintSubscription = {
  __typename: "Complaint";
  id: string;
  complaintId?: string | null;
  Date?: string | null;
  content?: string | null;
  user?: {
    __typename: "User";
    id: string;
    userId?: string | null;
    FirstName: string;
    LastName?: string | null;
    SUID: number;
    PhoneNumber?: number | null;
    username?: string | null;
    Email: string;
    Role?: string | null;
    schedule?: {
      __typename: "ModelScheduleConnection";
      nextToken?: string | null;
    } | null;
    complaint?: {
      __typename: "ModelComplaintConnection";
      nextToken?: string | null;
    } | null;
    feedback?: {
      __typename: "ModelFeedbackConnection";
      nextToken?: string | null;
    } | null;
    createdAt: string;
    updatedAt: string;
  } | null;
  createdAt: string;
  updatedAt: string;
};

export type OnDeleteComplaintSubscription = {
  __typename: "Complaint";
  id: string;
  complaintId?: string | null;
  Date?: string | null;
  content?: string | null;
  user?: {
    __typename: "User";
    id: string;
    userId?: string | null;
    FirstName: string;
    LastName?: string | null;
    SUID: number;
    PhoneNumber?: number | null;
    username?: string | null;
    Email: string;
    Role?: string | null;
    schedule?: {
      __typename: "ModelScheduleConnection";
      nextToken?: string | null;
    } | null;
    complaint?: {
      __typename: "ModelComplaintConnection";
      nextToken?: string | null;
    } | null;
    feedback?: {
      __typename: "ModelFeedbackConnection";
      nextToken?: string | null;
    } | null;
    createdAt: string;
    updatedAt: string;
  } | null;
  createdAt: string;
  updatedAt: string;
};

export type OnCreateFeedbackSubscription = {
  __typename: "Feedback";
  id: string;
  feedbackId?: string | null;
  Date?: string | null;
  content?: string | null;
  user?: {
    __typename: "User";
    id: string;
    userId?: string | null;
    FirstName: string;
    LastName?: string | null;
    SUID: number;
    PhoneNumber?: number | null;
    username?: string | null;
    Email: string;
    Role?: string | null;
    schedule?: {
      __typename: "ModelScheduleConnection";
      nextToken?: string | null;
    } | null;
    complaint?: {
      __typename: "ModelComplaintConnection";
      nextToken?: string | null;
    } | null;
    feedback?: {
      __typename: "ModelFeedbackConnection";
      nextToken?: string | null;
    } | null;
    createdAt: string;
    updatedAt: string;
  } | null;
  createdAt: string;
  updatedAt: string;
};

export type OnUpdateFeedbackSubscription = {
  __typename: "Feedback";
  id: string;
  feedbackId?: string | null;
  Date?: string | null;
  content?: string | null;
  user?: {
    __typename: "User";
    id: string;
    userId?: string | null;
    FirstName: string;
    LastName?: string | null;
    SUID: number;
    PhoneNumber?: number | null;
    username?: string | null;
    Email: string;
    Role?: string | null;
    schedule?: {
      __typename: "ModelScheduleConnection";
      nextToken?: string | null;
    } | null;
    complaint?: {
      __typename: "ModelComplaintConnection";
      nextToken?: string | null;
    } | null;
    feedback?: {
      __typename: "ModelFeedbackConnection";
      nextToken?: string | null;
    } | null;
    createdAt: string;
    updatedAt: string;
  } | null;
  createdAt: string;
  updatedAt: string;
};

export type OnDeleteFeedbackSubscription = {
  __typename: "Feedback";
  id: string;
  feedbackId?: string | null;
  Date?: string | null;
  content?: string | null;
  user?: {
    __typename: "User";
    id: string;
    userId?: string | null;
    FirstName: string;
    LastName?: string | null;
    SUID: number;
    PhoneNumber?: number | null;
    username?: string | null;
    Email: string;
    Role?: string | null;
    schedule?: {
      __typename: "ModelScheduleConnection";
      nextToken?: string | null;
    } | null;
    complaint?: {
      __typename: "ModelComplaintConnection";
      nextToken?: string | null;
    } | null;
    feedback?: {
      __typename: "ModelFeedbackConnection";
      nextToken?: string | null;
    } | null;
    createdAt: string;
    updatedAt: string;
  } | null;
  createdAt: string;
  updatedAt: string;
};

@Injectable({
  providedIn: "root"
})
export class APIService {
  async CreateUser(
    input: CreateUserInput,
    condition?: ModelUserConditionInput
  ): Promise<CreateUserMutation> {
    const statement = `mutation CreateUser($input: CreateUserInput!, $condition: ModelUserConditionInput) {
        createUser(input: $input, condition: $condition) {
          __typename
          id
          userId
          FirstName
          LastName
          SUID
          PhoneNumber
          username
          Email
          Role
          schedule {
            __typename
            items {
              __typename
              id
              day
              endTime
              month
              startTime
              year
              createdAt
              updatedAt
            }
            nextToken
          }
          complaint {
            __typename
            items {
              __typename
              id
              complaintId
              Date
              content
              createdAt
              updatedAt
            }
            nextToken
          }
          feedback {
            __typename
            items {
              __typename
              id
              feedbackId
              Date
              content
              createdAt
              updatedAt
            }
            nextToken
          }
          createdAt
          updatedAt
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    if (condition) {
      gqlAPIServiceArguments.condition = condition;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <CreateUserMutation>response.data.createUser;
  }
  async UpdateUser(
    input: UpdateUserInput,
    condition?: ModelUserConditionInput
  ): Promise<UpdateUserMutation> {
    const statement = `mutation UpdateUser($input: UpdateUserInput!, $condition: ModelUserConditionInput) {
        updateUser(input: $input, condition: $condition) {
          __typename
          id
          userId
          FirstName
          LastName
          SUID
          PhoneNumber
          username
          Email
          Role
          schedule {
            __typename
            items {
              __typename
              id
              day
              endTime
              month
              startTime
              year
              createdAt
              updatedAt
            }
            nextToken
          }
          complaint {
            __typename
            items {
              __typename
              id
              complaintId
              Date
              content
              createdAt
              updatedAt
            }
            nextToken
          }
          feedback {
            __typename
            items {
              __typename
              id
              feedbackId
              Date
              content
              createdAt
              updatedAt
            }
            nextToken
          }
          createdAt
          updatedAt
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    if (condition) {
      gqlAPIServiceArguments.condition = condition;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <UpdateUserMutation>response.data.updateUser;
  }
  async DeleteUser(
    input: DeleteUserInput,
    condition?: ModelUserConditionInput
  ): Promise<DeleteUserMutation> {
    const statement = `mutation DeleteUser($input: DeleteUserInput!, $condition: ModelUserConditionInput) {
        deleteUser(input: $input, condition: $condition) {
          __typename
          id
          userId
          FirstName
          LastName
          SUID
          PhoneNumber
          username
          Email
          Role
          schedule {
            __typename
            items {
              __typename
              id
              day
              endTime
              month
              startTime
              year
              createdAt
              updatedAt
            }
            nextToken
          }
          complaint {
            __typename
            items {
              __typename
              id
              complaintId
              Date
              content
              createdAt
              updatedAt
            }
            nextToken
          }
          feedback {
            __typename
            items {
              __typename
              id
              feedbackId
              Date
              content
              createdAt
              updatedAt
            }
            nextToken
          }
          createdAt
          updatedAt
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    if (condition) {
      gqlAPIServiceArguments.condition = condition;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <DeleteUserMutation>response.data.deleteUser;
  }
  async CreateSchedule(
    input: CreateScheduleInput,
    condition?: ModelScheduleConditionInput
  ): Promise<CreateScheduleMutation> {
    const statement = `mutation CreateSchedule($input: CreateScheduleInput!, $condition: ModelScheduleConditionInput) {
        createSchedule(input: $input, condition: $condition) {
          __typename
          id
          day
          endTime
          month
          startTime
          year
          user {
            __typename
            id
            userId
            FirstName
            LastName
            SUID
            PhoneNumber
            username
            Email
            Role
            schedule {
              __typename
              nextToken
            }
            complaint {
              __typename
              nextToken
            }
            feedback {
              __typename
              nextToken
            }
            createdAt
            updatedAt
          }
          createdAt
          updatedAt
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    if (condition) {
      gqlAPIServiceArguments.condition = condition;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <CreateScheduleMutation>response.data.createSchedule;
  }
  async UpdateSchedule(
    input: UpdateScheduleInput,
    condition?: ModelScheduleConditionInput
  ): Promise<UpdateScheduleMutation> {
    const statement = `mutation UpdateSchedule($input: UpdateScheduleInput!, $condition: ModelScheduleConditionInput) {
        updateSchedule(input: $input, condition: $condition) {
          __typename
          id
          day
          endTime
          month
          startTime
          year
          user {
            __typename
            id
            userId
            FirstName
            LastName
            SUID
            PhoneNumber
            username
            Email
            Role
            schedule {
              __typename
              nextToken
            }
            complaint {
              __typename
              nextToken
            }
            feedback {
              __typename
              nextToken
            }
            createdAt
            updatedAt
          }
          createdAt
          updatedAt
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    if (condition) {
      gqlAPIServiceArguments.condition = condition;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <UpdateScheduleMutation>response.data.updateSchedule;
  }
  async DeleteSchedule(
    input: DeleteScheduleInput,
    condition?: ModelScheduleConditionInput
  ): Promise<DeleteScheduleMutation> {
    const statement = `mutation DeleteSchedule($input: DeleteScheduleInput!, $condition: ModelScheduleConditionInput) {
        deleteSchedule(input: $input, condition: $condition) {
          __typename
          id
          day
          endTime
          month
          startTime
          year
          user {
            __typename
            id
            userId
            FirstName
            LastName
            SUID
            PhoneNumber
            username
            Email
            Role
            schedule {
              __typename
              nextToken
            }
            complaint {
              __typename
              nextToken
            }
            feedback {
              __typename
              nextToken
            }
            createdAt
            updatedAt
          }
          createdAt
          updatedAt
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    if (condition) {
      gqlAPIServiceArguments.condition = condition;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <DeleteScheduleMutation>response.data.deleteSchedule;
  }
  async CreateNews(
    input: CreateNewsInput,
    condition?: ModelNewsConditionInput
  ): Promise<CreateNewsMutation> {
    const statement = `mutation CreateNews($input: CreateNewsInput!, $condition: ModelNewsConditionInput) {
        createNews(input: $input, condition: $condition) {
          __typename
          id
          newsid
          Date
          Content
          createdAt
          updatedAt
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    if (condition) {
      gqlAPIServiceArguments.condition = condition;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <CreateNewsMutation>response.data.createNews;
  }
  async UpdateNews(
    input: UpdateNewsInput,
    condition?: ModelNewsConditionInput
  ): Promise<UpdateNewsMutation> {
    const statement = `mutation UpdateNews($input: UpdateNewsInput!, $condition: ModelNewsConditionInput) {
        updateNews(input: $input, condition: $condition) {
          __typename
          id
          newsid
          Date
          Content
          createdAt
          updatedAt
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    if (condition) {
      gqlAPIServiceArguments.condition = condition;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <UpdateNewsMutation>response.data.updateNews;
  }
  async DeleteNews(
    input: DeleteNewsInput,
    condition?: ModelNewsConditionInput
  ): Promise<DeleteNewsMutation> {
    const statement = `mutation DeleteNews($input: DeleteNewsInput!, $condition: ModelNewsConditionInput) {
        deleteNews(input: $input, condition: $condition) {
          __typename
          id
          newsid
          Date
          Content
          createdAt
          updatedAt
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    if (condition) {
      gqlAPIServiceArguments.condition = condition;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <DeleteNewsMutation>response.data.deleteNews;
  }
  async CreateComplaint(
    input: CreateComplaintInput,
    condition?: ModelComplaintConditionInput
  ): Promise<CreateComplaintMutation> {
    const statement = `mutation CreateComplaint($input: CreateComplaintInput!, $condition: ModelComplaintConditionInput) {
        createComplaint(input: $input, condition: $condition) {
          __typename
          id
          complaintId
          Date
          content
          user {
            __typename
            id
            userId
            FirstName
            LastName
            SUID
            PhoneNumber
            username
            Email
            Role
            schedule {
              __typename
              nextToken
            }
            complaint {
              __typename
              nextToken
            }
            feedback {
              __typename
              nextToken
            }
            createdAt
            updatedAt
          }
          createdAt
          updatedAt
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    if (condition) {
      gqlAPIServiceArguments.condition = condition;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <CreateComplaintMutation>response.data.createComplaint;
  }
  async UpdateComplaint(
    input: UpdateComplaintInput,
    condition?: ModelComplaintConditionInput
  ): Promise<UpdateComplaintMutation> {
    const statement = `mutation UpdateComplaint($input: UpdateComplaintInput!, $condition: ModelComplaintConditionInput) {
        updateComplaint(input: $input, condition: $condition) {
          __typename
          id
          complaintId
          Date
          content
          user {
            __typename
            id
            userId
            FirstName
            LastName
            SUID
            PhoneNumber
            username
            Email
            Role
            schedule {
              __typename
              nextToken
            }
            complaint {
              __typename
              nextToken
            }
            feedback {
              __typename
              nextToken
            }
            createdAt
            updatedAt
          }
          createdAt
          updatedAt
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    if (condition) {
      gqlAPIServiceArguments.condition = condition;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <UpdateComplaintMutation>response.data.updateComplaint;
  }
  async DeleteComplaint(
    input: DeleteComplaintInput,
    condition?: ModelComplaintConditionInput
  ): Promise<DeleteComplaintMutation> {
    const statement = `mutation DeleteComplaint($input: DeleteComplaintInput!, $condition: ModelComplaintConditionInput) {
        deleteComplaint(input: $input, condition: $condition) {
          __typename
          id
          complaintId
          Date
          content
          user {
            __typename
            id
            userId
            FirstName
            LastName
            SUID
            PhoneNumber
            username
            Email
            Role
            schedule {
              __typename
              nextToken
            }
            complaint {
              __typename
              nextToken
            }
            feedback {
              __typename
              nextToken
            }
            createdAt
            updatedAt
          }
          createdAt
          updatedAt
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    if (condition) {
      gqlAPIServiceArguments.condition = condition;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <DeleteComplaintMutation>response.data.deleteComplaint;
  }
  async CreateFeedback(
    input: CreateFeedbackInput,
    condition?: ModelFeedbackConditionInput
  ): Promise<CreateFeedbackMutation> {
    const statement = `mutation CreateFeedback($input: CreateFeedbackInput!, $condition: ModelFeedbackConditionInput) {
        createFeedback(input: $input, condition: $condition) {
          __typename
          id
          feedbackId
          Date
          content
          user {
            __typename
            id
            userId
            FirstName
            LastName
            SUID
            PhoneNumber
            username
            Email
            Role
            schedule {
              __typename
              nextToken
            }
            complaint {
              __typename
              nextToken
            }
            feedback {
              __typename
              nextToken
            }
            createdAt
            updatedAt
          }
          createdAt
          updatedAt
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    if (condition) {
      gqlAPIServiceArguments.condition = condition;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <CreateFeedbackMutation>response.data.createFeedback;
  }
  async UpdateFeedback(
    input: UpdateFeedbackInput,
    condition?: ModelFeedbackConditionInput
  ): Promise<UpdateFeedbackMutation> {
    const statement = `mutation UpdateFeedback($input: UpdateFeedbackInput!, $condition: ModelFeedbackConditionInput) {
        updateFeedback(input: $input, condition: $condition) {
          __typename
          id
          feedbackId
          Date
          content
          user {
            __typename
            id
            userId
            FirstName
            LastName
            SUID
            PhoneNumber
            username
            Email
            Role
            schedule {
              __typename
              nextToken
            }
            complaint {
              __typename
              nextToken
            }
            feedback {
              __typename
              nextToken
            }
            createdAt
            updatedAt
          }
          createdAt
          updatedAt
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    if (condition) {
      gqlAPIServiceArguments.condition = condition;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <UpdateFeedbackMutation>response.data.updateFeedback;
  }
  async DeleteFeedback(
    input: DeleteFeedbackInput,
    condition?: ModelFeedbackConditionInput
  ): Promise<DeleteFeedbackMutation> {
    const statement = `mutation DeleteFeedback($input: DeleteFeedbackInput!, $condition: ModelFeedbackConditionInput) {
        deleteFeedback(input: $input, condition: $condition) {
          __typename
          id
          feedbackId
          Date
          content
          user {
            __typename
            id
            userId
            FirstName
            LastName
            SUID
            PhoneNumber
            username
            Email
            Role
            schedule {
              __typename
              nextToken
            }
            complaint {
              __typename
              nextToken
            }
            feedback {
              __typename
              nextToken
            }
            createdAt
            updatedAt
          }
          createdAt
          updatedAt
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    if (condition) {
      gqlAPIServiceArguments.condition = condition;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <DeleteFeedbackMutation>response.data.deleteFeedback;
  }
  async GetUser(id: string): Promise<GetUserQuery> {
    const statement = `query GetUser($id: ID!) {
        getUser(id: $id) {
          __typename
          id
          userId
          FirstName
          LastName
          SUID
          PhoneNumber
          username
          Email
          Role
          schedule {
            __typename
            items {
              __typename
              id
              day
              endTime
              month
              startTime
              year
              createdAt
              updatedAt
            }
            nextToken
          }
          complaint {
            __typename
            items {
              __typename
              id
              complaintId
              Date
              content
              createdAt
              updatedAt
            }
            nextToken
          }
          feedback {
            __typename
            items {
              __typename
              id
              feedbackId
              Date
              content
              createdAt
              updatedAt
            }
            nextToken
          }
          createdAt
          updatedAt
        }
      }`;
    const gqlAPIServiceArguments: any = {
      id
    };
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <GetUserQuery>response.data.getUser;
  }
  async ListUsers(
    filter?: ModelUserFilterInput,
    limit?: number,
    nextToken?: string
  ): Promise<ListUsersQuery> {
    const statement = `query ListUsers($filter: ModelUserFilterInput, $limit: Int, $nextToken: String) {
        listUsers(filter: $filter, limit: $limit, nextToken: $nextToken) {
          __typename
          items {
            __typename
            id
            userId
            FirstName
            LastName
            SUID
            PhoneNumber
            username
            Email
            Role
            schedule {
              __typename
              nextToken
            }
            complaint {
              __typename
              nextToken
            }
            feedback {
              __typename
              nextToken
            }
            createdAt
            updatedAt
          }
          nextToken
        }
      }`;
    const gqlAPIServiceArguments: any = {};
    if (filter) {
      gqlAPIServiceArguments.filter = filter;
    }
    if (limit) {
      gqlAPIServiceArguments.limit = limit;
    }
    if (nextToken) {
      gqlAPIServiceArguments.nextToken = nextToken;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <ListUsersQuery>response.data.listUsers;
  }
  async GetSchedule(id: string): Promise<GetScheduleQuery> {
    const statement = `query GetSchedule($id: ID!) {
        getSchedule(id: $id) {
          __typename
          id
          day
          endTime
          month
          startTime
          year
          user {
            __typename
            id
            userId
            FirstName
            LastName
            SUID
            PhoneNumber
            username
            Email
            Role
            schedule {
              __typename
              nextToken
            }
            complaint {
              __typename
              nextToken
            }
            feedback {
              __typename
              nextToken
            }
            createdAt
            updatedAt
          }
          createdAt
          updatedAt
        }
      }`;
    const gqlAPIServiceArguments: any = {
      id
    };
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <GetScheduleQuery>response.data.getSchedule;
  }
  async ListSchedules(
    filter?: ModelScheduleFilterInput,
    limit?: number,
    nextToken?: string
  ): Promise<ListSchedulesQuery> {
    const statement = `query ListSchedules($filter: ModelScheduleFilterInput, $limit: Int, $nextToken: String) {
        listSchedules(filter: $filter, limit: $limit, nextToken: $nextToken) {
          __typename
          items {
            __typename
            id
            day
            endTime
            month
            startTime
            year
            user {
              __typename
              id
              userId
              FirstName
              LastName
              SUID
              PhoneNumber
              username
              Email
              Role
              createdAt
              updatedAt
            }
            createdAt
            updatedAt
          }
          nextToken
        }
      }`;
    const gqlAPIServiceArguments: any = {};
    if (filter) {
      gqlAPIServiceArguments.filter = filter;
    }
    if (limit) {
      gqlAPIServiceArguments.limit = limit;
    }
    if (nextToken) {
      gqlAPIServiceArguments.nextToken = nextToken;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <ListSchedulesQuery>response.data.listSchedules;
  }
  async GetNews(id: string): Promise<GetNewsQuery> {
    const statement = `query GetNews($id: ID!) {
        getNews(id: $id) {
          __typename
          id
          newsid
          Date
          Content
          createdAt
          updatedAt
        }
      }`;
    const gqlAPIServiceArguments: any = {
      id
    };
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <GetNewsQuery>response.data.getNews;
  }
  async ListNewss(
    filter?: ModelNewsFilterInput,
    limit?: number,
    nextToken?: string
  ): Promise<ListNewssQuery> {
    const statement = `query ListNewss($filter: ModelNewsFilterInput, $limit: Int, $nextToken: String) {
        listNewss(filter: $filter, limit: $limit, nextToken: $nextToken) {
          __typename
          items {
            __typename
            id
            newsid
            Date
            Content
            createdAt
            updatedAt
          }
          nextToken
        }
      }`;
    const gqlAPIServiceArguments: any = {};
    if (filter) {
      gqlAPIServiceArguments.filter = filter;
    }
    if (limit) {
      gqlAPIServiceArguments.limit = limit;
    }
    if (nextToken) {
      gqlAPIServiceArguments.nextToken = nextToken;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <ListNewssQuery>response.data.listNewss;
  }
  async GetComplaint(id: string): Promise<GetComplaintQuery> {
    const statement = `query GetComplaint($id: ID!) {
        getComplaint(id: $id) {
          __typename
          id
          complaintId
          Date
          content
          user {
            __typename
            id
            userId
            FirstName
            LastName
            SUID
            PhoneNumber
            username
            Email
            Role
            schedule {
              __typename
              nextToken
            }
            complaint {
              __typename
              nextToken
            }
            feedback {
              __typename
              nextToken
            }
            createdAt
            updatedAt
          }
          createdAt
          updatedAt
        }
      }`;
    const gqlAPIServiceArguments: any = {
      id
    };
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <GetComplaintQuery>response.data.getComplaint;
  }
  async ListComplaints(
    filter?: ModelComplaintFilterInput,
    limit?: number,
    nextToken?: string
  ): Promise<ListComplaintsQuery> {
    const statement = `query ListComplaints($filter: ModelComplaintFilterInput, $limit: Int, $nextToken: String) {
        listComplaints(filter: $filter, limit: $limit, nextToken: $nextToken) {
          __typename
          items {
            __typename
            id
            complaintId
            Date
            content
            user {
              __typename
              id
              userId
              FirstName
              LastName
              SUID
              PhoneNumber
              username
              Email
              Role
              createdAt
              updatedAt
            }
            createdAt
            updatedAt
          }
          nextToken
        }
      }`;
    const gqlAPIServiceArguments: any = {};
    if (filter) {
      gqlAPIServiceArguments.filter = filter;
    }
    if (limit) {
      gqlAPIServiceArguments.limit = limit;
    }
    if (nextToken) {
      gqlAPIServiceArguments.nextToken = nextToken;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <ListComplaintsQuery>response.data.listComplaints;
  }
  async GetFeedback(id: string): Promise<GetFeedbackQuery> {
    const statement = `query GetFeedback($id: ID!) {
        getFeedback(id: $id) {
          __typename
          id
          feedbackId
          Date
          content
          user {
            __typename
            id
            userId
            FirstName
            LastName
            SUID
            PhoneNumber
            username
            Email
            Role
            schedule {
              __typename
              nextToken
            }
            complaint {
              __typename
              nextToken
            }
            feedback {
              __typename
              nextToken
            }
            createdAt
            updatedAt
          }
          createdAt
          updatedAt
        }
      }`;
    const gqlAPIServiceArguments: any = {
      id
    };
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <GetFeedbackQuery>response.data.getFeedback;
  }
  async ListFeedbacks(
    filter?: ModelFeedbackFilterInput,
    limit?: number,
    nextToken?: string
  ): Promise<ListFeedbacksQuery> {
    const statement = `query ListFeedbacks($filter: ModelFeedbackFilterInput, $limit: Int, $nextToken: String) {
        listFeedbacks(filter: $filter, limit: $limit, nextToken: $nextToken) {
          __typename
          items {
            __typename
            id
            feedbackId
            Date
            content
            user {
              __typename
              id
              userId
              FirstName
              LastName
              SUID
              PhoneNumber
              username
              Email
              Role
              createdAt
              updatedAt
            }
            createdAt
            updatedAt
          }
          nextToken
        }
      }`;
    const gqlAPIServiceArguments: any = {};
    if (filter) {
      gqlAPIServiceArguments.filter = filter;
    }
    if (limit) {
      gqlAPIServiceArguments.limit = limit;
    }
    if (nextToken) {
      gqlAPIServiceArguments.nextToken = nextToken;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <ListFeedbacksQuery>response.data.listFeedbacks;
  }
  OnCreateUserListener: Observable<
    SubscriptionResponse<OnCreateUserSubscription>
  > = API.graphql(
    graphqlOperation(
      `subscription OnCreateUser {
        onCreateUser {
          __typename
          id
          userId
          FirstName
          LastName
          SUID
          PhoneNumber
          username
          Email
          Role
          schedule {
            __typename
            items {
              __typename
              id
              day
              endTime
              month
              startTime
              year
              createdAt
              updatedAt
            }
            nextToken
          }
          complaint {
            __typename
            items {
              __typename
              id
              complaintId
              Date
              content
              createdAt
              updatedAt
            }
            nextToken
          }
          feedback {
            __typename
            items {
              __typename
              id
              feedbackId
              Date
              content
              createdAt
              updatedAt
            }
            nextToken
          }
          createdAt
          updatedAt
        }
      }`
    )
  ) as Observable<SubscriptionResponse<OnCreateUserSubscription>>;

  OnUpdateUserListener: Observable<
    SubscriptionResponse<OnUpdateUserSubscription>
  > = API.graphql(
    graphqlOperation(
      `subscription OnUpdateUser {
        onUpdateUser {
          __typename
          id
          userId
          FirstName
          LastName
          SUID
          PhoneNumber
          username
          Email
          Role
          schedule {
            __typename
            items {
              __typename
              id
              day
              endTime
              month
              startTime
              year
              createdAt
              updatedAt
            }
            nextToken
          }
          complaint {
            __typename
            items {
              __typename
              id
              complaintId
              Date
              content
              createdAt
              updatedAt
            }
            nextToken
          }
          feedback {
            __typename
            items {
              __typename
              id
              feedbackId
              Date
              content
              createdAt
              updatedAt
            }
            nextToken
          }
          createdAt
          updatedAt
        }
      }`
    )
  ) as Observable<SubscriptionResponse<OnUpdateUserSubscription>>;

  OnDeleteUserListener: Observable<
    SubscriptionResponse<OnDeleteUserSubscription>
  > = API.graphql(
    graphqlOperation(
      `subscription OnDeleteUser {
        onDeleteUser {
          __typename
          id
          userId
          FirstName
          LastName
          SUID
          PhoneNumber
          username
          Email
          Role
          schedule {
            __typename
            items {
              __typename
              id
              day
              endTime
              month
              startTime
              year
              createdAt
              updatedAt
            }
            nextToken
          }
          complaint {
            __typename
            items {
              __typename
              id
              complaintId
              Date
              content
              createdAt
              updatedAt
            }
            nextToken
          }
          feedback {
            __typename
            items {
              __typename
              id
              feedbackId
              Date
              content
              createdAt
              updatedAt
            }
            nextToken
          }
          createdAt
          updatedAt
        }
      }`
    )
  ) as Observable<SubscriptionResponse<OnDeleteUserSubscription>>;

  OnCreateScheduleListener: Observable<
    SubscriptionResponse<OnCreateScheduleSubscription>
  > = API.graphql(
    graphqlOperation(
      `subscription OnCreateSchedule {
        onCreateSchedule {
          __typename
          id
          day
          endTime
          month
          startTime
          year
          user {
            __typename
            id
            userId
            FirstName
            LastName
            SUID
            PhoneNumber
            username
            Email
            Role
            schedule {
              __typename
              nextToken
            }
            complaint {
              __typename
              nextToken
            }
            feedback {
              __typename
              nextToken
            }
            createdAt
            updatedAt
          }
          createdAt
          updatedAt
        }
      }`
    )
  ) as Observable<SubscriptionResponse<OnCreateScheduleSubscription>>;

  OnUpdateScheduleListener: Observable<
    SubscriptionResponse<OnUpdateScheduleSubscription>
  > = API.graphql(
    graphqlOperation(
      `subscription OnUpdateSchedule {
        onUpdateSchedule {
          __typename
          id
          day
          endTime
          month
          startTime
          year
          user {
            __typename
            id
            userId
            FirstName
            LastName
            SUID
            PhoneNumber
            username
            Email
            Role
            schedule {
              __typename
              nextToken
            }
            complaint {
              __typename
              nextToken
            }
            feedback {
              __typename
              nextToken
            }
            createdAt
            updatedAt
          }
          createdAt
          updatedAt
        }
      }`
    )
  ) as Observable<SubscriptionResponse<OnUpdateScheduleSubscription>>;

  OnDeleteScheduleListener: Observable<
    SubscriptionResponse<OnDeleteScheduleSubscription>
  > = API.graphql(
    graphqlOperation(
      `subscription OnDeleteSchedule {
        onDeleteSchedule {
          __typename
          id
          day
          endTime
          month
          startTime
          year
          user {
            __typename
            id
            userId
            FirstName
            LastName
            SUID
            PhoneNumber
            username
            Email
            Role
            schedule {
              __typename
              nextToken
            }
            complaint {
              __typename
              nextToken
            }
            feedback {
              __typename
              nextToken
            }
            createdAt
            updatedAt
          }
          createdAt
          updatedAt
        }
      }`
    )
  ) as Observable<SubscriptionResponse<OnDeleteScheduleSubscription>>;

  OnCreateNewsListener: Observable<
    SubscriptionResponse<OnCreateNewsSubscription>
  > = API.graphql(
    graphqlOperation(
      `subscription OnCreateNews {
        onCreateNews {
          __typename
          id
          newsid
          Date
          Content
          createdAt
          updatedAt
        }
      }`
    )
  ) as Observable<SubscriptionResponse<OnCreateNewsSubscription>>;

  OnUpdateNewsListener: Observable<
    SubscriptionResponse<OnUpdateNewsSubscription>
  > = API.graphql(
    graphqlOperation(
      `subscription OnUpdateNews {
        onUpdateNews {
          __typename
          id
          newsid
          Date
          Content
          createdAt
          updatedAt
        }
      }`
    )
  ) as Observable<SubscriptionResponse<OnUpdateNewsSubscription>>;

  OnDeleteNewsListener: Observable<
    SubscriptionResponse<OnDeleteNewsSubscription>
  > = API.graphql(
    graphqlOperation(
      `subscription OnDeleteNews {
        onDeleteNews {
          __typename
          id
          newsid
          Date
          Content
          createdAt
          updatedAt
        }
      }`
    )
  ) as Observable<SubscriptionResponse<OnDeleteNewsSubscription>>;

  OnCreateComplaintListener: Observable<
    SubscriptionResponse<OnCreateComplaintSubscription>
  > = API.graphql(
    graphqlOperation(
      `subscription OnCreateComplaint {
        onCreateComplaint {
          __typename
          id
          complaintId
          Date
          content
          user {
            __typename
            id
            userId
            FirstName
            LastName
            SUID
            PhoneNumber
            username
            Email
            Role
            schedule {
              __typename
              nextToken
            }
            complaint {
              __typename
              nextToken
            }
            feedback {
              __typename
              nextToken
            }
            createdAt
            updatedAt
          }
          createdAt
          updatedAt
        }
      }`
    )
  ) as Observable<SubscriptionResponse<OnCreateComplaintSubscription>>;

  OnUpdateComplaintListener: Observable<
    SubscriptionResponse<OnUpdateComplaintSubscription>
  > = API.graphql(
    graphqlOperation(
      `subscription OnUpdateComplaint {
        onUpdateComplaint {
          __typename
          id
          complaintId
          Date
          content
          user {
            __typename
            id
            userId
            FirstName
            LastName
            SUID
            PhoneNumber
            username
            Email
            Role
            schedule {
              __typename
              nextToken
            }
            complaint {
              __typename
              nextToken
            }
            feedback {
              __typename
              nextToken
            }
            createdAt
            updatedAt
          }
          createdAt
          updatedAt
        }
      }`
    )
  ) as Observable<SubscriptionResponse<OnUpdateComplaintSubscription>>;

  OnDeleteComplaintListener: Observable<
    SubscriptionResponse<OnDeleteComplaintSubscription>
  > = API.graphql(
    graphqlOperation(
      `subscription OnDeleteComplaint {
        onDeleteComplaint {
          __typename
          id
          complaintId
          Date
          content
          user {
            __typename
            id
            userId
            FirstName
            LastName
            SUID
            PhoneNumber
            username
            Email
            Role
            schedule {
              __typename
              nextToken
            }
            complaint {
              __typename
              nextToken
            }
            feedback {
              __typename
              nextToken
            }
            createdAt
            updatedAt
          }
          createdAt
          updatedAt
        }
      }`
    )
  ) as Observable<SubscriptionResponse<OnDeleteComplaintSubscription>>;

  OnCreateFeedbackListener: Observable<
    SubscriptionResponse<OnCreateFeedbackSubscription>
  > = API.graphql(
    graphqlOperation(
      `subscription OnCreateFeedback {
        onCreateFeedback {
          __typename
          id
          feedbackId
          Date
          content
          user {
            __typename
            id
            userId
            FirstName
            LastName
            SUID
            PhoneNumber
            username
            Email
            Role
            schedule {
              __typename
              nextToken
            }
            complaint {
              __typename
              nextToken
            }
            feedback {
              __typename
              nextToken
            }
            createdAt
            updatedAt
          }
          createdAt
          updatedAt
        }
      }`
    )
  ) as Observable<SubscriptionResponse<OnCreateFeedbackSubscription>>;

  OnUpdateFeedbackListener: Observable<
    SubscriptionResponse<OnUpdateFeedbackSubscription>
  > = API.graphql(
    graphqlOperation(
      `subscription OnUpdateFeedback {
        onUpdateFeedback {
          __typename
          id
          feedbackId
          Date
          content
          user {
            __typename
            id
            userId
            FirstName
            LastName
            SUID
            PhoneNumber
            username
            Email
            Role
            schedule {
              __typename
              nextToken
            }
            complaint {
              __typename
              nextToken
            }
            feedback {
              __typename
              nextToken
            }
            createdAt
            updatedAt
          }
          createdAt
          updatedAt
        }
      }`
    )
  ) as Observable<SubscriptionResponse<OnUpdateFeedbackSubscription>>;

  OnDeleteFeedbackListener: Observable<
    SubscriptionResponse<OnDeleteFeedbackSubscription>
  > = API.graphql(
    graphqlOperation(
      `subscription OnDeleteFeedback {
        onDeleteFeedback {
          __typename
          id
          feedbackId
          Date
          content
          user {
            __typename
            id
            userId
            FirstName
            LastName
            SUID
            PhoneNumber
            username
            Email
            Role
            schedule {
              __typename
              nextToken
            }
            complaint {
              __typename
              nextToken
            }
            feedback {
              __typename
              nextToken
            }
            createdAt
            updatedAt
          }
          createdAt
          updatedAt
        }
      }`
    )
  ) as Observable<SubscriptionResponse<OnDeleteFeedbackSubscription>>;
}
