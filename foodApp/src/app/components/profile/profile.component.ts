import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  isEditEnable : boolean = true; // to show and hide the edit button
  isUpdatePassword : boolean = false; //to show and hide the update password form
  sentCode: boolean = false;

  constructor(private route: ActivatedRoute, private router: Router,) { }

  ProfilePageForm: FormGroup = new FormGroup({
    username: new FormControl('', [Validators.required]),
    suid: new FormControl('', [Validators.required]),
    emailId: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required]),
    phoneNumber: new FormControl('', [Validators.required]),
  });

  resetForm: FormGroup = new FormGroup({
    username: new FormControl('', [Validators.required]),
    code: new FormControl('', [Validators.required]),
    newPassword: new FormControl('',[Validators.required])
  });

  
  ngOnInit(): void {
  }

  // this is temp


  onBack(){
    this.router.navigate(['/schedule']);
  }


  logOut(){
    this.router.navigate(['/login']);
  }
  onClickClose(){

  };
  onEdit(){
    this.isEditEnable =!this.isEditEnable;
  }
  onEditProfileCancel(){
    this.isEditEnable =!this.isEditEnable;
  }
  onEditProfileSubmit(){}
  onUpdatePassword(){
    this.router.navigate(['/update-password']);
  }
  
  onSendCode(){
    this.sentCode = true;
  }

  onHome(){
    this.router.navigate(['/dashboard']);
  }
  




}
