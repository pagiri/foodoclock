import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Auth } from 'aws-amplify';
import { APIService } from '../../API.service';
import { ConfirmDialogModel, ConfirmDialogComponent } from '../confirm-dialog/confirm-dialog.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup = new FormGroup({
    username: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required]),
  });

  get username() { return this.loginForm.get('username') };
  get password() { return this.loginForm.get('password') };
  usersList: String[];
  result: string = '';
  errorMessage="";
  loading = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private api: APIService,
    public dialog: MatDialog) { }

  ngOnInit(): void {

  }

  async login() {
    const users = await this.api.ListUsers();
    console.log(users);
    this.loading = true;
    try {
      const cognitoUser = await Auth.signIn(this.username.value, this.password.value);
      if (cognitoUser) {
        this.loading = false;
        const isUserAllowed = users.items.filter(x => {
          if (x.Email == cognitoUser.attributes.email) {
            return x;
          }
        })
        if (isUserAllowed.length > 0) {
          this.router.navigate(['/dashboard']);
        } else {
          this.confirmDialog();
        }
      }
    } catch (e) {
      this.loading = false;
      if(e.code == "InvalidParameterException"){
        this.errorMessage = "Password cannot be empty!"
      }else{
      this.errorMessage = e.message;
      }
    }
  }

  confirmDialog(): void {
    const message = `You don't have permissions. Please contact the admin!`;

    const dialogData = new ConfirmDialogModel("Confirm Action", message);

    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      maxWidth: "400px",
      data: dialogData
    });

    dialogRef.afterClosed().subscribe(dialogResult => {
      this.result = dialogResult;
    });
  }
}


