import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {
  sentCode=false;
  loading=false;

  resetForm: FormGroup = new FormGroup({
    username: new FormControl('', [Validators.required]),
    code: new FormControl('', [Validators.required]),
    newPassword: new FormControl('',[Validators.required])
  });

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  sendCode(){
    this.sentCode = true;
  }

}
