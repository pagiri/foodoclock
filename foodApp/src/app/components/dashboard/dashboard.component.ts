import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Auth } from 'aws-amplify';
import { APIService } from '../../API.service';
import { ConfirmDialogModel, ConfirmDialogComponent } from '../confirm-dialog/confirm-dialog.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  tiles = ["Profile", "Schedule", "News", "Feedback"]
  result = "";
  constructor(private route: ActivatedRoute,
    private router: Router,
    private api: APIService,
    public dialog: MatDialog) { }

  ngOnInit(): void {
  }

  onProfile(){
    this.router.navigate(['/profile']);
  }

  onSchedule(){
    this.router.navigate(['/schedule']);
  }

  onNews(){
    this.router.navigate(['/news']);
  }

  async logOut(){
    console.log("aaa");
    try {
      await Auth.signOut();
       this.router.navigate(['/login']);
  } catch (error) {
    this.confirmDialog();
  }
  }
  onFeedback(){
    this.router.navigate(['/feedback']);
  }

  confirmDialog(): void {
    const message = `Error Signing Out!`;

    const dialogData = new ConfirmDialogModel("Confirm Action", message);

    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      maxWidth: "400px",
      data: dialogData
    });

    dialogRef.afterClosed().subscribe(dialogResult => {
      this.result = dialogResult;
    });
  }
}
