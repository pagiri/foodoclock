import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CalenderDialogComponent } from './calender-dialog.component';

describe('CalenderDialogComponent', () => {
  let component: CalenderDialogComponent;
  let fixture: ComponentFixture<CalenderDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CalenderDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CalenderDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
