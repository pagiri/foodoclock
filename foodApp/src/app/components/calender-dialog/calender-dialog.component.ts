import { Component, Inject, OnInit, ViewChild, ElementRef } from '@angular/core';
import { MatDialogRef, MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import {MatTableModule} from '@angular/material/table';
import {MatChipInputEvent} from '@angular/material/chips';
import {COMMA, ENTER} from '@angular/cdk/keycodes';

export interface PeriodicElement {
  symbol: string[];
  time: string;
}
const ELEMENT_DATA: PeriodicElement[] = [
  {time: "7-30:10 am", symbol: ['Dwayne Jhonson','Tom cruise']},
  {time: "9-11:30 am", symbol: ['Kevin peterson', 'Brett Lee']},
  {time: "1-4 pm", symbol: ['Priyanka', 'Hema']},
  {time: "5:9 pm", symbol: ['Pallavi', 'Nirutha']},
];
@Component({
  selector: 'app-calender-dialog',
  templateUrl: './calender-dialog.component.html',
  styleUrls: ['./calender-dialog.component.scss']
})
export class CalenderDialogComponent implements OnInit {
  message: string = "Schedule!!"
  confirmButtonText = "Yes"
  cancelButtonText = "Cancel"

  visible = true;
  selectable = true;
  removable = true;
  separatorKeysCodes: number[] = [ENTER, COMMA];
  // symbolCtrl = new FormControl();

  displayedColumns: string[] = ['time', 'symbol'];
  dataSource = ELEMENT_DATA;

  @ViewChild('fruitInput') fruitInput: ElementRef<HTMLInputElement>;

  constructor(
    @Inject(MAT_DIALOG_DATA) private data: any,
    private dialogRef: MatDialogRef<CalenderDialogComponent>) {
      if(data){
    this.message = data.message || this.message;
    if (data.buttonText) {
      this.confirmButtonText = data.buttonText.ok || this.confirmButtonText;
      this.cancelButtonText = data.buttonText.cancel || this.cancelButtonText;
    }
      }
  }

  onConfirmClick(): void {
    this.dialogRef.close(true);
  }

  ngOnInit(){
  }

  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    // Add our fruit
    if ((value || '').trim()) {
      ELEMENT_DATA['symbol'].push(value.trim());
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }

    // this.fruitCtrl.setValue(null);
  }

  remove(fruit: string): void {
    // const index = this.fruits.indexOf(fruit);

    // if (index >= 0) {
    //   this.fruits.splice(index, 1);
    // }
  }

}
