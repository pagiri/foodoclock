import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import {Auth} from 'aws-amplify';
import { APIService } from '../../API.service';
import { ConfirmDialogModel, ConfirmDialogComponent } from '../confirm-dialog/confirm-dialog.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {
  errorMessage = "";
  loading = false;
  signUpForm: FormGroup = new FormGroup({
    username: new FormControl('', [Validators.required]),
    suid: new FormControl('', [Validators.required]),
    email: new FormControl('', Validators.compose([Validators.required,
      // Validators.pattern("[^ @]*@[^ @]*"),
      CodeCraftValidators.emailDomain])),
    password: new FormControl('', [Validators.required, Validators.minLength(8)]),
    phoneNumber: new FormControl('', [Validators.required, Validators.min(10)]),
  });

  get username(){return this.signUpForm.get('username')};
  get suid(){return this.signUpForm.get('suid')};
  get email(){return this.signUpForm.get('email')};
  get password(){return this.signUpForm.get('password')};
  get phoneNumber(){return this.signUpForm.get('phoneNumber')};

  account_validation_messages = {
    'username': [
      { type: 'required', message: 'Username is required' },
     // { type: 'validUsername', message: this.errorMessage }
    ],
    'email': [
      { type: 'emailDomain', message: 'Email should belong to the domain "syr.edu"' },
      { type: 'required', message: 'Email is required' },
      { type: 'pattern', message: 'Enter a valid email' },
    ],
    'password': [
      { type: 'required', message: 'Password is required' },
      { type: 'minlength', message: 'Password must be at least 8 characters long' }
    ],
    'suid': [
      { type: 'required', message: 'Suid is required' },
      { type: 'min', message: 'Suid must be at least 10 characters long' }
    ],
    'phoneNumber': [
      { type: 'required', message: 'Phone number is required' },
      { type: 'min', message: 'Phone number must be at least 10 characters long' }
    ]
    }
  constructor( private route: ActivatedRoute,
    private router: Router,
    private api: APIService,
    public dialog: MatDialog) { }

  ngOnInit(): void {
    console.log("aaa");
  }

  async signUp(){
    this.loading = true;
    if (this.signUpForm.valid) {
        const user1 = this.username.value;
        const pass = this.password.value;
        
        const email1 = this.email.value.toString();
        const phone = "+1" + this.phoneNumber.value.toString();
        const suid1 = this.suid.value.toString();
      
      try {
        this.loading = false;
        const { user } = await Auth.signUp({
          username:user1,
          password:pass,
          attributes: {
              email:email1,         
              phone_number:phone,  
              // other custom attributes 
              'custom:SUID':suid1
          }
        });
        this.router.navigate(['/confirmSignUp'],{queryParams:{val:this.username.value}});
        // this.router.navigate(['/forgotPassword']);
    } catch (error) {
      this.loading = false;
        console.log('error signing up:', error);
    }
    } else {
      // validate all form fields
      this.validateAllFormFields(this.signUpForm);
    }
  }

  validateAllFormFields(formGroup: FormGroup) {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof FormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }

}



export class CodeCraftValidators {
  static emailDomain(fc: FormControl){
    if(fc.value != "" && fc.value.split('@')[1] != 'syr.edu'){
      return ({
        emailDomain: true});
    } else {
      return (null);
    }
  }
}