import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import {Auth} from 'aws-amplify';
import { APIService } from '../../../API.service';
import { ConfirmDialogModel, ConfirmDialogComponent } from '../../confirm-dialog/confirm-dialog.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-confirm-sign-up',
  templateUrl: './confirm-sign-up.component.html',
  styleUrls: ['./confirm-sign-up.component.scss']
})

export class ConfirmSignUpComponent implements OnInit {
@Input() code: string = '';
errorMessage = '';
username = "";
result: string = '';

  constructor(private activeroute:ActivatedRoute, private route:Router,
    private api: APIService,
    public dialog: MatDialog) { }

  ngOnInit(){
    this.activeroute.queryParams.subscribe(params=>{
      this.username = params['val'];
    })
  }
async submitCode(){
  try{
   await Auth.confirmSignUp(this.username, this.code);

   this.confirmDialog();
  }catch(e){
    this.errorMessage = e.message;
  }
}

confirmDialog(): void {
  const message = `Sign Up Successful!`;

  const dialogData = new ConfirmDialogModel("Confirm Action", message);

  const dialogRef = this.dialog.open(ConfirmDialogComponent, {
    maxWidth: "400px",
    data: dialogData
  });

  dialogRef.afterClosed().subscribe(dialogResult => {
    this.result = dialogResult;
    this.route.navigate(['/login']);
  });
}
}
